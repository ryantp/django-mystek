from django.conf import settings
from django.conf.urls import url
from django.contrib.staticfiles.urls import static, staticfiles_urlpatterns

from . import views

app_name = 'site_news'

urlpatterns = [
	# index page to news: https://mystek.com/news/ # this is a list page
    url(r'^$',
    	views.index,
    	name = 'index'),

    # create news article: https://mystek.com/news/create-article/
    url(r'^create-article/',
    	views.create_article,
    	name = 'create_article'),

    url(r'^edit-article/(?P<slug>[0-9a-zA-Z\-_]+)/$',
        views.edit_article,
        name = 'edit_article'),

    # detail view, using slug: https://mystek.com/news/<slug>/
    url(r'^(?P<slug>[0-9a-zA-Z-_]+)/',
    	views.article_detail,
    	name = 'article_detail'),

    
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns == static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)