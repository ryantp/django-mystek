from django.forms import ModelForm

from .models import Article

class WriteNewsArticleForm(ModelForm):
	class Meta:
		model = Article
		fields = [
			'title',
			'image',
			'content',
			'keywords',
			'is_public',
		]
	# title = forms.CharField(
	# 	max_length = 150,
	# 	required = True,
	# 	label = 'Aritcle Title')
	# image = forms.ImageField(
	# 	required = False,
	# 	label = 'Related Image')
	# content = forms.TextField(
	# 	max_length = 5000,
	# 	required = True,
	# 	label = 'Article Content')
	# is_public = forms.BooleanField(
	# 	label = "Public Article",
	# 	default = False)


