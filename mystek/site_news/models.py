from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify

# Create your models here.


# class Tags(models.Model):
# 	title = models.CharField(
# 		max_length = 50,
# 		blank = False,
# 		verbose_name = 'Tag Title')
# 	slug = models.SlugField(
# 		max_length = 50,
# 		unique = True)

# 	def __str__(self):
# 		return "Tag %(t)s" % {'t': self.title}


class Article(models.Model):
	title = models.CharField(
		max_length = 150,
		blank = False,
		verbose_name = 'Article Title')
	slug = models.SlugField(
		max_length = 50,
		unique = True)
	author = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		blank = False,
		null = False,
		on_delete = models.CASCADE,)
	pub_date = models.DateTimeField(auto_now_add = True)
	last_mod = models.DateTimeField(auto_now = True)
	image = models.ImageField(
		upload_to = 'media/news/',
		blank = True,
		null = True,
		verbose_name = 'Related Image')
	content = models.TextField(
		max_length = 5000,
		blank = False,
		default = '',
		verbose_name = 'Article Content')
	keywords = models.TextField(
		max_length = 500,
		blank = True,
		default = '',
		verbose_name = "Article Keywords")
	# if is_public, non-logged ppl can view the article
	is_public = models.BooleanField(
		default = False,
		verbose_name = 'Publicly Viewable Article')

	def __str__(self):
		return "Article: \"%(t)s\" by %(u)s on %(d)s" % {
			't': self.title[:20],
			'u': self.author.username,
			'd': self.pub_date.strftime("%Y-%m-%d@%H:%M:%S")
		}


def create_slug(instance, new_slug = None):
	slug = slugify(instance.title)
	if new_slug is not None:
		slug = new_slug
	qs = Article.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" % (slug, qs.first().id)
		return create_slug(instance, new_slug = new_slug)
	return slug

def pre_save_article(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

pre_save.connect(pre_save_article, sender = Article)
