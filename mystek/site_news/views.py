from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.utils import timezone

from .models import Article
from .forms import WriteNewsArticleForm

# from mystek.settings import MEDIA_URL

# Create your views here.

def index(request):
	# a list of links to news stories
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		context['logged_in'] = False
	if request.user.is_authenticated:
		article_list = Article.objects.all().order_by("-pub_date")
	else:
		article_list = [x for x in Article.objects.all().order_by("-pub_date") if x.is_public]
	paginator = Paginator(article_list, 10) # show 10 news articles per page

	page = request.GET.get('page')
	try:
		articles = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver first page
		articles = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of results
		articles = paginator.page(paginator.num_pages)

	context['articles'] = articles

	return render(request, 'site_news/index.html', context)

@login_required
def create_article(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = False
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('site_news.add_article'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		# redirect user to login before proceeding
		return HttpResponseRedirect(url = '/login/')
	# if POST
	if request.method == 'POST':
		form = WriteNewsArticleForm(
			data = request.POST,
			files = request.FILES)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.author = request.user
			article.pub_date = timezone.now()
			article.last_mod = timezone.now()
			article.save()
			return HttpResponseRedirect('/news/')
	else:
		context['form'] = WriteNewsArticleForm()
	return render(request, 'site_news/make-article.html', context)

@login_required
def edit_article(request, slug):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = True
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
		article_instance = Article.objects.get(slug = slug)

		if context['user'].has_perm('site_news.change_article'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		# redirect user to login before proceeding
		return HttpResponseRedirect(url = '/login/')
	# if POST
	if request.method == 'POST':
		form = WriteNewsArticleForm(
			data = request.POST,
			files = request.FILES,
			instance = article_instance)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			# article.author = request.user
			# article.pub_date = timezone.now()
			article.last_mod = timezone.now()
			article.save()
			return HttpResponseRedirect('/news/')
	else:
		context['form'] = WriteNewsArticleForm(instance = article_instance)
	return render(request, 'site_news/make-article.html', context)

# no login is required to read site news articles, if the article is flagged 'public'
def article_detail(request, slug):
	context = dict()
	try:
		article = Article.objects.filter(slug = slug)[0]
	except IndexError:
		article = None

	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
		context['is_avail'] = True
		context['article'] = article
	else:
		context['logged_in'] = False
		if article.is_public:
			context['is_avail'] = True
			context['article'] = article
		else:
			context['article_slug'] = article.slug

	return render(request, 'site_news/article-detail.html', context)
