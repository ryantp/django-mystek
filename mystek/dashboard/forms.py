from django.forms import ModelForm

from .models import Story, Todo


class StoryForm(ModelForm):
	class Meta:
		model = Story
		fields = [
			'title',
			'chapter',
			'content'
		]


class TodoForm(ModelForm):
	class Meta:
		model = Todo
		fields = [
			'content',
		]
