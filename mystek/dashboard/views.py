from django.conf import settings
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.utils import timezone

from .forms import StoryForm, TodoForm
from .models import Story, Todo

from site_news.models import Article

# Create your views here.
@login_required
def change_passw(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = PasswordChangeForm(request.user, request.POST)
		if form.is_valid():
			_user = form.save()
			update_session_auth_hash(request, _user)
			return redirect('/')
	else:
		context['form'] = PasswordChangeForm(request.user)
	return render(request, 'dashboard/change-passw.html', context)

@login_required
def iperm(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpsResponseRedirect('/login/')

	return render(request, 'dashboard/denied.html', context)

@login_required
def story_create(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('dashboard.add_story'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = StoryForm(data = request.POST)

		# check if valid
		if form.is_valid():
			# process the data
			story = form.save(commit = False)
			story.pub_date = timezone.now()
			story.last_mod = timezone.now()
			story.save()
			return HttpResponseRedirect('/dashboard/story/')
	else:
		context['form'] = StoryForm() # if request == GET, send blank template

	return render(request, 'dashboard/story_create.html', context)

# template -- complete
# ugly, but it works
@login_required
def index(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	news_articles = Article.objects.filter(
		author = request.user).order_by('-pub_date')[0:5] # display (up to) 5 news articles

	if news_articles:
		context['news_articles'] = news_articles

	context['todos'] = Todo.objects.filter(
		user = request.user).order_by('-pub_date')[0:5] # display (up to) 5 todo notes


	# print("\n**********")
	# print(request.user.id)
	# print("**********\n")

	return render(request, 'dashboard/index.html', context)

# CLUSTER: story
@login_required
def story_list(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	item_list = Story.objects.all()
	paginator = Paginator(item_list, 30) # show 30 item listings at once

	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver first page
		items = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of results
		items = paginator.page(paginator.num_pages)

	context['entries'] = items

	return render(request, 'dashboard/story_list.html', context)

@login_required
def story_detail(request, slug):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	context['story'] = Story.objects.get(slug = slug)


	return render(request, 'dashboard/story_detail.html', context)

@login_required
def todo_create(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('dashboard.add_todo'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	#if POST
	if request.method == 'POST':
		form = TodoForm(data = request.POST)

		if form.is_valid():
			todo_note = form.save(commit = False)
			todo_note.user = request.user
			todo_note.pub_date = timezone.now()
			todo_note.save()
			return HttpResponseRedirect('/dashboard/')
	else:
		context['form'] = TodoForm()

	return render(request, 'dashboard/todo_create.html', context)

