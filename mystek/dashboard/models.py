from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify

# Create your models here.
# two models here: todo and story


class Todo(models.Model):
	pub_date = models.DateTimeField(auto_now_add = True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL,
		blank = False,
		null = False,
		on_delete = models.CASCADE)
	content = models.TextField(
		max_length = 400,
		verbose_name = 'To Do Note')

	def __str__(self):
		return "To Do: %(d)s" % {
		'd': self.pub_date.strftime("%Y-%m-%d@%H:%M:%S")}


class Story(models.Model):
	title = models.CharField(
		max_length = 100,
		blank = False,
		default = '',
		verbose_name = 'Story Entry')
	slug = models.SlugField(max_length = 50, unique = True)
	chapter = models.CharField(
		max_length = 10,
		blank = False,
		default = '1',
		verbose_name = 'Story Chapter')
	content = models.TextField(
		max_length = 10000,
		blank = False,
		default = '',
		verbose_name = 'Content')
	pub_date = models.DateTimeField(auto_now_add = True)
	last_mod = models.DateTimeField(auto_now = True)

	def __str__(self):
		return "Story -- Chapter %(n)s" % {'n': self.chapter}

def create_slug(instance, new_slug = None):
	slug = slugify(instance.title)
	if new_slug is not None:
		slug = new_slug
	qs = Story.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" % (slug, qs.first().id)
		return create_slug(instance, new_slug = new_slug)
	return slug

def pre_save_story(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

pre_save.connect(pre_save_story, sender = Story)