# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-13 05:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todo',
            name='content',
            field=models.TextField(max_length=400, verbose_name=b'To Do Note'),
        ),
    ]
