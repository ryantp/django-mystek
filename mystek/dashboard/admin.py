from django.contrib import admin

from .models import Story, Todo

# Register your models here.
admin.site.register(Story)
admin.site.register(Todo)
