from django.conf.urls import url

from . import views

app_name = 'dashboard'

urlpatterns = [
	# https://mystek.com/dashboard/
    url(r'^$', views.index, name = 'index'),

    # STORY CLUSTER
    # https://mystek.com/dashboard/story/
    url(r'^story/$', views.story_list, name = 'story_list'),
    # https://mystek.com/dashboard/story/create/
    url(r'^story/create/$', views.story_create, name = 'story_create'),
    # https://mystek.com/dashboard/story/<slug>/
    url(r'^story/(?P<slug>[0-9a-zA-Z\-_]+)/$', views.story_detail, name = 'story_detail'),

    # OTHER CLUSTER
    # https://mystek.net/dashboard/todo/
    url(r'^todo/$', views.todo_create, name = 'todo_create'),
    # https://mystek.net/dashboard/change-passw/
    url(r'^change-passw/$', views.change_passw, name = 'change_passw'),

    # CUSTOM DENIAL URL
    url(r'^iperm/$', views.iperm, name = 'iperm'),
]
