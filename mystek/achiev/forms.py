from django.forms import ModelForm

from registry.models import Achievement


class AchievementForm(ModelForm):
	class Meta:
		model = Achievement
		fields = [
			'name',
			'unique_id',
			'in_game_description',
			'description',
		]
