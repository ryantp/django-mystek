from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import AchievementForm

from registry.models import Achievement

# Create your views here.
########################
# CLUSTER 1: Achievement
########################
@login_required
def achiev_list(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	item_list = Achievement.objects.all()
	paginator = Paginator(item_list, 30) # show 30 item listings at once

	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver first page
		items = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of results
		items = paginator.page(paginator.num_pages)

	context['objects_list'] = items

	return render(request, 'achiev/index.html', context)

@login_required
def achiev_help(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
		if context['user'].has_perm('registry.add_achievement'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'achiev/achiev-help.html', context)

@login_required
def achiev_detail(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['achiev'] = Achievement.objects.get(unique_id = uid)

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'achiev/achiev_detail.html', context)

@login_required
def achiev_create(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = False
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_achievement'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = AchievementForm(data = request.POST)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.save()
			return HttpResponseRedirect('/achiev/')

	else:
		context['form'] = AchievementForm()

	return render(request, 'achiev/achiev_create.html', context)

@login_required
def achiev_edit(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = True

	achiev_instance = Achievement.objects.get(unique_id = uid)
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.change_achievement'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = AchievementForm(
			data = request.POST,
			instance = achiev_instance)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.is_active = False
			article.save()
			return HttpResponseRedirect('/achiev/')

	else:
		context['form'] = AchievementForm(instance = achiev_instance)

	return render(request, 'achiev/achiev_create.html', context)
