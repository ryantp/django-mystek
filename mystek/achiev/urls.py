from django.conf.urls import url

from . import views

app_name = 'achiev'

urlpatterns = [
	# ACHIEV CLUSTER
    # https://mystek.com/dashboard/achiev/
    url(r'^$', views.achiev_list, name = 'achiev_list'),
    # https://mystek.com/dashboard/achiev/<uid>/
    url(r'^(?P<uid>X[0-9]+)/$', views.achiev_detail, name = 'achiev_detail'),
    # https://mystek.com/dashboard/achiev/create/
    url(r'^create/$', views.achiev_create, name = 'achiev_create'),
    # https://mystek.com/achiev/edit
    url(r'^edit/(?P<uid>X[0-9]+)/$', views.achiev_edit, name = 'achiev_edit'),
    url(r'^help/$', views.achiev_help, name = 'achiev_help'),
]
