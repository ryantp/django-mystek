from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import NPCForm

from registry.models import NPC

# Create your views here.
#####################
# CLUSTER 5: NPC
#####################
@login_required
def npc_list(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	item_list = NPC.objects.all()
	paginator = Paginator(item_list, 30) # show 30 item listings at once

	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver first page
		items = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of results
		items = paginator.page(paginator.num_pages)

	context['objects_list'] = items

	return render(request, 'npc/npc-list.html', context)

@login_required
def npc_help(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_npc'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'npc/npc-help.html', context)

@login_required
def npc_detail(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	context['item'] = NPC.objects.get(character_id = uid)

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'npc/npc-detail.html', context)

@login_required
def npc_create(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = False
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_npc'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = NPCForm(data = request.POST)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.save()
			return HttpResponseRedirect('/npc/')

	else:
		context['form'] = NPCForm()

	return render(request, 'npc/npc-create.html', context)

@login_required
def npc_edit(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = True

	npc_instance = NPC.objects.get(character_id = uid)
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.change_npc'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = NPCForm(
			data = request.POST,
			instance = npc_instance)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.save()
			return HttpResponseRedirect('/npc/')

	else:
		context['form'] = NPCForm(instance = npc_instance)

	return render(request, 'npc/npc-create.html', context)
