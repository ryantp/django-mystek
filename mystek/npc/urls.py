from django.conf.urls import url

from . import views

app_name = 'npc'

urlpatterns = [
	# NPC CLUSTER
    # https://mystek.com/npc/
    url(r'^$', views.npc_list, name = 'npc_list'),
    # https://mystek.com/npc/<uid>/
    url(r'^(?P<uid>0x[0-9A-F]+)/$', views.npc_detail, name = 'npc_detail'),
    # https://mystek.com/npc/<uid>/
    url(r'^create/$', views.npc_create, name = 'npc_create'),
    # https://mystek.com/npc/edit/<uid>
    url(r'^edit/(?P<uid>0x[0-9A-F]+)/$', views.npc_edit, name = 'npc_edit'),
    url(r'^help/$', views.npc_help, name = 'npc_help'),
]
