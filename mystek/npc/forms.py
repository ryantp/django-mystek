from django.forms import ModelForm

from registry.models import NPC


class NPCForm(ModelForm):
	class Meta:
		model = NPC
		fields = [
			'name',
			'character_id',
			'abst_world',
			'description',
			'character_dialog',
			'reward_items',
			'reward_items_trigger',
		]
