from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import KeyItemForm

from registry.models import KeyItem

# Create your views here.
######################
# CLUSTER 4: KeyItem
######################
@login_required
def keyitem_list(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	item_list = KeyItem.objects.all()
	paginator = Paginator(item_list, 30) # show 30 item listings at once

	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver first page
		items = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of results
		items = paginator.page(paginator.num_pages)

	context['objects_list'] = items

	return render(request, 'keyitem/keyitem-list.html', context)

@login_required
def keyitem_help(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_keyitem'):
			pass
		else:
			return HttpResponseRedirect("/dashboard/iperm/")
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'keyitem/keyitem-help.html', context)

@login_required
def keyitem_detail(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['item'] = KeyItem.objects.get(unique_id = uid)

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'keyitem/keyitem-detail.html', context)

@login_required
def keyitem_create(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = False
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_keyitem'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = KeyItemForm(data = request.POST)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.save()
			return HttpResponseRedirect('/keyitem/')

	else:
		context['form'] = KeyItemForm()

	return render(request, 'keyitem/keyitem-create.html', context)

@login_required
def keyitem_edit(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = True

	keyitem_instance = KeyItem.objects.get(unique_id = uid)
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.change_keyitem'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = KeyItemForm(
			data = request.POST,
			instance = keyitem_instance)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.save()
			return HttpResponseRedirect('/keyitem/')

	else:
		context['form'] = KeyItemForm(instance = keyitem_instance)

	return render(request, 'keyitem/keyitem-create.html', context)

