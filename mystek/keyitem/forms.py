from django.forms import ModelForm

from registry.models import KeyItem


class KeyItemForm(ModelForm):
	class Meta:
		model = KeyItem
		fields = [
			'name',
			'unique_id',
			'in_game_description',
			'description',
			'drop_weight',
			'buy_price',
			'sell_price',
		]
