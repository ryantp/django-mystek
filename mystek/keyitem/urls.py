from django.conf.urls import url

from . import views

app_name = 'keyitem'

urlpatterns = [
	# KEYITEM CLUSTER
    # https://mystek.com/dashboard/
    url(r'^$', views.keyitem_list, name = 'keyitem_list'),
    # https://mystek.com/dashboard/<uid>
    url(r'^(?P<uid>0x[0-9A-F]+)/$', views.keyitem_detail, name = 'keyitem_detail'),
    # https://mystek.com/dashboard/create/
    url(r'^create/$', views.keyitem_create, name = 'keyitem_create'),
    # https://mystek.com/keyitem/edit/<uid>
    url(r'^edit/(?P<uid>0x[0-9A-F]+)/$', views.keyitem_edit, name = 'keyitem_edit'),
    url(r'^help/$', views.keyitem_help, name = 'keyitem_help'),
]
