from django.conf.urls import url

from . import views

app_name = 'armwep'

urlpatterns = [
	# ARMOR & WEAPON CLUSTER
    # https://mystek.com/dashboard/armwep/
    url(r'^$', views.armwep_list, name = 'armwep_list'),
    # https://mystek.com/dashboard/armwep/<uid>
    url(r'^(?P<uid>0x[0-9A-F]+)/$', views.armwep_detail, name = 'armwep_detail'),
    # https://mystek.com/dashboard/armwep/create
    url(r'^create/$', views.armwep_create, name = 'armwep_create'),
    url(r'^edit/(?P<uid>0x[0-9A-F]+)/$', views.armwep_edit, name = 'armwep_edit'),
    url(r'^help/$', views.armwep_help, name = 'armwep_help'),
]
