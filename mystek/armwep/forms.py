from django.forms import ModelForm

from registry.models import ArmorWeapon


class ArmWepForm(ModelForm):
	class Meta:
		model = ArmorWeapon
		fields = [
			'name',
			'unique_id',
			'in_game_description',
			'description',
			'item_type',
			'item_type2',
			'target',
			'target2',
			'drop_weight',
			'effect_value',
			'status_effect',
			'buy_price',
			'sell_price',
		]
