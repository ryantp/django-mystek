from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import ArmWepForm

from registry.models import ArmorWeapon

# Create your views here.
########################
# CLUSTER 2: ArmorWeapon
########################
@login_required
def armwep_list(request):	# finisihed 1/2
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	item_list = ArmorWeapon.objects.all()
	paginator = Paginator(item_list, 30) # show 30 item listings at once

	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver first page
		items = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of results
		items = paginator.page(paginator.num_pages)

	context['armweps'] = items

	return render(request, 'armwep/armwep-list.html', context)

@login_required
def armwep_help(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_armorweapon'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'armwep/armwep-help.html', context)

@login_required
def armwep_detail(request, uid):	# finisihed 1/2
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['aw'] = ArmorWeapon.objects.get(unique_id = uid)

	if context['aw'].effect_value > 0:
		context['polar_val'] = 'pos-val'
		context['pol_sym'] = "+"
	elif context['aw'].effect_value < 0:
		context['polar_val'] = 'neg-val'
		context['pol_sym'] = "-"
	else:
		context['polar_val'] = 'zro-val'
		context['pol_sym'] = "&plusmn;"

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'armwep/armwep-detail.html', context)

@login_required
def armwep_create(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = False
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_armorweapon'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = ArmWepForm(data = request.POST)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.is_active = False
			article.save()
			return HttpResponseRedirect('/armwep/')

	else:
		context['form'] = ArmWepForm()

	return render(request, 'armwep/armwep-create.html', context)

@login_required
def armwep_edit(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = True

	armwep_instance = ArmorWeapon.objects.get(unique_id = uid)
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.change_armorweapon'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = ArmWepForm(
			data = request.POST,
			instance = armwep_instance)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.is_active = False
			article.save()
			return HttpResponseRedirect('/armwep/')

	else:
		context['form'] = ArmWepForm(instance = armwep_instance)

	return render(request, 'armwep/armwep-create.html', context)
