from django.conf.urls import url

from . import views

app_name = 'stateff'

urlpatterns = [
	 # STATEFF CLUSTER
    # https://mystek.com/stateff
    url(r'^$', views.stateff_list, name = 'stateff_list'),
    # https://mystek.com/stateff/<uid>/
    url(r'^(?P<uid>0x[0-9A-F]+)/$', views.stateff_detail, name = 'stateff_detail'),
    # https://mystek.com/stateff/create/
    url(r'^create/$', views.stateff_create, name = 'stateff_create'),
    # https://mystek.com/stateff/edit/<uid>/
    url(r'^edit/(?P<uid>0x[0-9A-F]+)/$', views.stateff_edit, name = 'stateff_edit'),
    url(r'^help/$', views.stateff_help, name = 'stateff_help'),
]
