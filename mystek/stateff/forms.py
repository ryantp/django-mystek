from django.forms import ModelForm

from registry.models import StatusEffect


class StatusEffectForm(ModelForm):
	class Meta:
		model = StatusEffect
		fields = [
			'name',
			'unique_id',
			'abst_type',
			'in_game_description',
			'description',
			'duration',
			'target',
			'effect_value',
		]
