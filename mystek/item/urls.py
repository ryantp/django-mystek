from django.conf.urls import url

from . import views

app_name = 'item'

urlpatterns = [
	# ITEM CLUSTER
    # https://mystek.com/dashboard/item/
    url(r'^$', views.item_list, name = 'item_list'),
    # https://mystek.com/dashboard/item/<uid>/ ## uid always starts with 0x
    url(r'^(?P<uid>0x[0-9A-F]+)/$', views.item_detail, name = 'item_detail'),
    # https://mystek.com/dashboard/item/create/
    url(r'^create/$', views.item_create, name = 'item_create'),
    # https://mystek.com/armwep/edit/<uid>/
    url(r'^edit/(?P<uid>0x[0-9A-F]+)/$', views.item_edit, name = 'item_edit'),
    url(r'^help/$', views.item_help, name = 'item_help'),
]
