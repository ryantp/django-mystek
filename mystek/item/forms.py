from django.forms import ModelForm

from registry.models import Item


class ItemForm(ModelForm):
	class Meta:
		model = Item
		fields = [
			'name',
			'unique_id',
			'in_game_description',
			'description',
			'item_type',
			'target',
			'target2',
			'drop_weight',
			'effect_value',
			'status_effect',
			'buy_price',
			'sell_price',
		]
