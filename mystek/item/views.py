from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import ItemForm

from registry.models import Item

# Create your views here.
######################
# CLUSTER 3: Item
######################
@login_required
def item_list(request):		#finished 1/2
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	item_list = Item.objects.all()
	paginator = Paginator(item_list, 30) # show 30 item listings at once

	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver first page
		items = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of results
		items = paginator.page(paginator.num_pages)

	context['objects_list'] = items

	return render(request, 'item/item-list.html', context)

@login_required
def item_help(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_item'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'item/item-help.html', context)

@login_required
def item_detail(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['item'] = Item.objects.get(unique_id = uid)

	if context['item'].effect_value > 0:
		context['polar_val'] = 'pos-val'
		context['pol_sym'] = "+"
	elif context['item'].effect_value < 0:
		context['polar_val'] = 'neg-val'
		context['pol_sym'] = "-"
	else:
		context['polar_val'] = 'zro-val'
		context['pol_sym'] = "#177;"

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		return HttpResponseRedirect('/login/')

	return render(request, 'item/item-detail.html', context)

@login_required
def item_create(request):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = False

	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.add_item'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = ItemForm(data = request.POST)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.save()
			return HttpResponseRedirect('/item/')

	else:
		context['form'] = ItemForm()

	return render(request, 'item/item-create.html', context)

@login_required
def item_edit(request, uid):
	context = dict()

	context['MEDIA_URL'] = settings.MEDIA_URL
	context['edit_model'] = True

	item_instance = Item.objects.get(unique_id = uid)
	
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user

		if context['user'].has_perm('registry.change_item'):
			pass
		else:
			return HttpResponseRedirect('/dashboard/iperm/')
	else:
		return HttpResponseRedirect('/login/')

	# if POST
	if request.method == 'POST':
		form = ItemForm(
			data = request.POST,
			instance = item_instance)

		# check if valid
		if form.is_valid():
			# process the data
			article = form.save(commit = False)
			article.save()
			return HttpResponseRedirect('/item/')

	else:
		context['form'] = ItemForm(instance = item_instance)

	return render(request, 'item/item-create.html', context)

