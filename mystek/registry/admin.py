from django.contrib import admin

from .models import Achievement, ArmorWeapon, Item, KeyItem, NPC, StatusEffect

# Register your models here.
admin.site.register(Achievement)
admin.site.register(ArmorWeapon)
admin.site.register(Item)
admin.site.register(KeyItem)
admin.site.register(NPC)
admin.site.register(StatusEffect)

