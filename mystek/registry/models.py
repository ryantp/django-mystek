from django.db import models

# Create your models here.


class StatusEffect(models.Model):
	abst_type = models.CharField(
		max_length = 15,
		blank = False,
		verbose_name = "Status Effect Type")
	unique_id = models.CharField(
		max_length = 6,
		blank = True,
		default = '0xB000',
		unique = True,
		verbose_name = "Status Effect Unique ID")
	name = models.CharField(
		max_length = 140,
		verbose_name = "Status Effect Name")
	in_game_description = models.TextField(
		max_length = 200,
		default = '',
		verbose_name = "In-Game Description")
	description = models.TextField(
		max_length = 300,
		blank = True,
		default = '',
		verbose_name = "Description")
	duration = models.IntegerField(
		null = False,
		verbose_name = "Status Effect Duration")
	target = models.CharField(
		max_length = 15,
		blank = False,
		verbose_name = "Status Effect Target")
	effect_value = models.IntegerField(
		null = False,
		verbose_name = "Status Effect Value")

	def __str__(self):
		return "%(a)s Status Effect: %(n)s" % {
			'a': self.abst_type.title(),
			'n': self.name
		}


class ArmorWeapon(models.Model):
	unique_id = models.CharField(
		max_length = 6,
		blank = True,
		default = '0xA000',
		unique = True,
		verbose_name = "Armor / Weapon Unique ID")
	name = models.CharField(
		max_length = 140,
		verbose_name = "Armor / Weapon Name")
	in_game_description = models.TextField(
		max_length = 200,
		default = '',
		verbose_name = "In-Game Description")
	description = models.TextField(
		max_length = 300,
		blank = True,
		default = '',
		verbose_name = "Description")
	item_type = models.CharField(
		max_length = 15,
		blank = False,
		default = 'weapon',
		verbose_name = "Armor / Weapon Type[1]")
	item_type2 = models.CharField(
		max_length = 15,
		blank = False,
		default = 'sword',
		verbose_name = "Armor / Weapon Type[2]")
	target = models.CharField(
		max_length = 10,
		blank = False,
		default = 'player',
		verbose_name = 'Armor / Weapon Target[1]')
	target2 = models.CharField(
		max_length = 10,
		blank = False,
		default = 'health',
		verbose_name = 'Armor / Weapon Target[2]')
	drop_weight = models.IntegerField(
		null = False,
		verbose_name = "Armor / Weapon Drop Rate [n/1000]")
	effect_value = models.IntegerField(
		null = False,
		verbose_name = "Armor / Weapon Stat Value")
	status_effect = models.ForeignKey(
		StatusEffect,
		blank = True,
		null = True,
		on_delete = models.CASCADE,
		verbose_name = "Armor / Weapon Status Effect (optional)")
	buy_price = models.FloatField(
		null = True,
		verbose_name = "Buying Price")
	sell_price = models.FloatField(
		null = True,
		verbose_name = "Selling Price")
	item_quantity = models.IntegerField(null = False, default = 1)

	def __str__(self):
		return "%(t)s Item: %(n)s" % {
			't': self.item_type.title(),
			'n': self.name
		}


class Item(models.Model):
	unique_id = models.CharField(
		max_length = 6,
		unique = True,
		default = '0x1000',
		verbose_name = "Item Unique ID")
	name = models.CharField(
		max_length = 140,
		verbose_name = "Item Name")
	in_game_description = models.TextField(
		max_length = 200,
		default = '',
		verbose_name = "In-Game Description")
	description = models.TextField(
		max_length = 300,
		blank = True,
		default = '',
		verbose_name = "Description")
	item_type = models.CharField(
		max_length = 15,
		blank = False,
		default = 'health',
		verbose_name = "Item Type")
	target = models.CharField(
		max_length = 10,
		blank = False,
		default = 'player',
		verbose_name = "Item Target[1]")
	target2 = models.CharField(
		max_length = 10,
		blank = False,
		default = 'hp',
		verbose_name = "Item Target[2]")
	drop_weight = models.IntegerField(
		null = False,
		verbose_name = "Item Drop Rate [n/1000]")
	effect_value = models.IntegerField(
		null = False,
		verbose_name = "Item Stat Value")
	status_effect = models.ForeignKey(
		StatusEffect,
		blank = True,
		null = True,
		on_delete = models.CASCADE,
		verbose_name = "Item Status Effect")
	buy_price = models.FloatField(
		null = True,
		verbose_name = "Buying Price")
	sell_price = models.FloatField(
		null = True,
		verbose_name = "Selling Price")
	item_quantity = models.IntegerField(null = False, default = 1)

	def __str__(self):
		return "%(t)s Item: %(n)s" % {
			't': self.item_type.title(),
			'n': self.name
		}


class KeyItem(models.Model):
	unique_id = models.CharField(
		max_length = 6,
		blank = True,
		default = '0x1100',
		unique = True,
		verbose_name = "KeyItem Unique ID")
	name = models.CharField(
		max_length = 140,
		verbose_name = "KeyItem Name")
	in_game_description = models.TextField(
		max_length = 200,
		default = '',
		verbose_name = "In-Game Description")
	description = models.TextField(
		max_length = 300,
		blank = True,
		default = '',
		verbose_name = "Description")
	item_type = models.CharField(
		max_length = 7,
		blank = False,
		default = 'keyitem')
	drop_weight = models.IntegerField(
		null = False,
		verbose_name = "KeyItem Drop Rate")
	buy_price = models.FloatField(
		null = True,
		verbose_name = "Buying Price")
	sell_price = models.FloatField(
		null = True,
		verbose_name = "Selling Price")
	item_quantity = models.IntegerField(null = False, default = 1)
	is_activated = models.BooleanField(null = False, default = False)

	def __str__(self):
		return "%(t)s Item: %(n)s" % {
			't': self.item_type.title(),
			'n': self.name
		}


class NPC(models.Model):
	abst_world = models.CharField(
		max_length = 5,
		default = "W0-1",
		verbose_name = "Character Spawn World")
	character_id = models.CharField(
		max_length = 6,
		blank = False,
		default = '0xC000',
		unique = True,
		verbose_name = "Character Unique ID")
	name = models.CharField(
		max_length = 50,
		blank = False,
		default = 'Bob',
		verbose_name = "Character's Name")
	description = models.TextField(
		max_length = 1000,
		blank = True,
		default = '',
		verbose_name = "Description")
	spawn_default = models.CharField(
		max_length = 9,
		blank = True,
		default = "0,0")
	spawn_current = models.CharField(
		max_length = 9,
		blank = True,
		default = "0,0")
	character_dialog = models.TextField(
		max_length = 10000,
		default = "",
		verbose_name = "Character Dialog")
	reward_items = models.ManyToManyField(
		Item,
		blank = True,
		null = True,
		verbose_name = "Rewardable Items")
	reward_items_trigger = models.CharField(
		max_length = 100,
		blank = True,
		default = '0x1001:3',
		verbose_name = "Rewardable Items Trigger")
	is_ri_avail = models.BooleanField(null = False, default = True)
	is_npc_avail = models.BooleanField(null = False, default = True)

	def __str__(self):
		return "NPC: %(n)s" % {'n': self.name}


class Achievement(models.Model):
	name = models.CharField(
		max_length = 100,
		unique = True,
		verbose_name = "Achievement Name")
	unique_id = models.CharField(
		max_length = 4,
		unique = True,
		default = 'X001',
		verbose_name = "Achievement Unique ID")
	in_game_description = models.TextField(
		max_length = 200,
		default = '',
		verbose_name = "In-Game Description")
	description = models.TextField(
		max_length = 300,
		blank = True,
		default = '')
	is_active = models.BooleanField(
		blank = True,
		null = False,
		default = False)

	def __str__(self):
		return "Achievement: %(n)s" % {'n': self.name}


#
