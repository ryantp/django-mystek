from django.conf import settings
from django.shortcuts import render

from site_news.models import Article

# Create your views here.

def index(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
		context['news'] = Article.objects.all().order_by('-pub_date')[:5]
	else:
		context['logged_in'] = False
		articles = [x for x in Article.objects.all().order_by('-pub_date') if x.is_public]
		context['news'] = articles
	return render(request, 'home/index.html', context)

# def site_about(request):
# 	pass

# def site_security(request):
# 	pass

# def about_passwords(request):
# 	pass
