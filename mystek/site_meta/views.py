from django.conf import settings
from django.shortcuts import render

# Create your views here.
def site_about(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		context['logged_in'] = False
	return render(request, 'site_meta/site-about.html', context)

def site_security(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		context['logged_in'] = False
	return render(request, 'site_meta/site-security.html', context)

def about_passwords(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		context['logged_in'] = False
	return render(request, 'site_meta/about-passwords.html', context)

def site_proj(request):
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		context['logged_in'] = False
	return render(request, 'site_meta/about-projects.html', context)

def site_news(request):
	# DEPRECIATE; go to site_news/index
	context = dict()
	context['MEDIA_URL'] = settings.MEDIA_URL
	if request.user.is_authenticated:
		context['logged_in'] = True
		context['user'] = request.user
	else:
		context['logged_in'] = False
	return render(request, 'site_meta/site-news.html', context)


