from django.conf.urls import url

from . import views

app_name = 'site_meta'

urlpatterns = [
	# https://mystek.com/about/
	url(r'^$', views.site_about, name = 'site_about'),

	# https://mystek.com/about/security/
    url(r'^security/', views.site_security, name = 'site_security'),

    # https://mystek.com/about/security/passwords/
    url(r'^security/passwords', views.about_passwords, name = 'about_passw'),

    # https://mystek.com/about/projects/
    url(r'^projects/', views.site_proj, name = 'site_proj'),

    # https://mystek.com/about/news/
    url(r'^news/', views.site_news, name = 'site_news'),
]