from django.apps import AppConfig


class SiteMetaConfig(AppConfig):
    name = 'site_meta'
